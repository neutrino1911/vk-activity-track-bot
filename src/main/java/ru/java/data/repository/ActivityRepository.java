package ru.java.data.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import ru.java.data.document.Activity;

import java.util.List;

public interface ActivityRepository extends MongoRepository<Activity, Long> {

    @Query(value = "{'_id.peerId': ?0}")
    List<Activity> findAllByPeerIdOrderByDate(Integer peerId);

}
