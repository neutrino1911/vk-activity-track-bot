package ru.java.data.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.java.data.document.Chat;

public interface ChatRepository extends MongoRepository<Chat, Integer> {

}
