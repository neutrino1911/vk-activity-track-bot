package ru.java.data.document;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public class Activity {

    @Id
    private ActivityId id;

    private Integer date;

    private Integer messageId;

    public Integer getPeerId() {
        return id.peerId;
    }

    public Integer getFromId() {
        return id.fromId;
    }

    @Getter
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    public static class ActivityId {

        private Integer peerId;

        private Integer fromId;

        public static ActivityId of(Integer peerId, Integer fromId) {
            return new ActivityId(peerId, fromId);
        }

    }

}
