package ru.java.data.document;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public class Chat {

    @Id
    private Integer peerId;

    private String title;

}
