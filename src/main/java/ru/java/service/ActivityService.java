package ru.java.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.java.data.document.Activity;
import ru.java.data.repository.ActivityRepository;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class ActivityService {

    private final ActivityRepository activityRepository;

    public String printStat(int peerId, int days) {
        var activities = activityRepository.findAllByPeerIdOrderByDate(peerId);
        return activities.stream().map(it -> getMention(it, days)).collect(Collectors.joining("\n"));
    }

    private String getMention(Activity activity, int days) {
        var bound = LocalDateTime.now().minusDays(days);
        var dateTime = LocalDateTime.ofEpochSecond(activity.getDate(), 0, ZoneOffset.ofHours(3));
        return String.format("%s @id%d %s",
                dateTime.isBefore(bound) ? "☠️" : "",
                activity.getFromId(),
                DateTimeFormatter.ISO_DATE_TIME.format(dateTime)
        );
    }

}
