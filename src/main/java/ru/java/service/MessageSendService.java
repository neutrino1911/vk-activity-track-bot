package ru.java.service;

import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.GroupActor;
import com.vk.api.sdk.objects.messages.Keyboard;
import com.vk.api.sdk.objects.messages.Message;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.concurrent.ThreadLocalRandom;

@Slf4j
@Service
@RequiredArgsConstructor
public class MessageSendService {

    private final VkApiClient vkApiClient;

    private final GroupActor groupActor;

    @SneakyThrows
    public void send(Message message, String text) {
        var send = vkApiClient.messages().send(groupActor);
        send.randomId(ThreadLocalRandom.current().nextInt())
            .peerId(message.getPeerId())
            .disableMentions(true)
            .message(text);
        send.execute();
    }

    @SneakyThrows
    public void send(Message message, String text, Keyboard keyboard) {
        var send = vkApiClient.messages().send(groupActor);
        send.randomId(ThreadLocalRandom.current().nextInt())
            .peerId(message.getPeerId())
            .disableMentions(true)
            .keyboard(keyboard)
            .message(text);
        send.execute();
    }

}
