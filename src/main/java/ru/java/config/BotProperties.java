package ru.java.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("vk.bot")
public class BotProperties {

    private Integer groupId;

    private String token;

}
