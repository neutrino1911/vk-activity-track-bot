package ru.java.config;

import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.GroupActor;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import ru.java.component.CallbackApiLongPollHandler;

@Configuration
@EnableScheduling
@EnableConfigurationProperties(BotProperties.class)
@EnableMongoRepositories("ru.java.data.repository")
public class Config {

    @Bean
    public VkApiClient vkApiClient() {
        return new VkApiClient(HttpTransportClient.getInstance());
    }

    @Bean
    public GroupActor groupActor(BotProperties props) {
        return new GroupActor(props.getGroupId(), props.getToken());
    }

    @Bean
    public CallbackApiLongPollHandler pollHandler(BotProperties props) {
        return new CallbackApiLongPollHandler(vkApiClient(), groupActor(props));
    }

}
