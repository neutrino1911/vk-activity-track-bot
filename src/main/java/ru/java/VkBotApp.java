package ru.java;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VkBotApp {

    public static void main(String[] args) {
        SpringApplication.run(VkBotApp.class, args);
    }

}
