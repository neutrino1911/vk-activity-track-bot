package ru.java.component;

import com.vk.api.sdk.callback.longpoll.CallbackApiLongPoll;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.GroupActor;
import com.vk.api.sdk.objects.messages.Message;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.java.component.event.NewMessageEvent;

@Slf4j
@Component
public class CallbackApiLongPollHandler extends CallbackApiLongPoll {

    @Autowired
    private ApplicationEventPublisher publisher;

    public CallbackApiLongPollHandler(VkApiClient client, GroupActor actor) {
        super(client, actor);
    }

    @SneakyThrows
    @Scheduled(fixedDelay = 1000)
    public void start() {
        try {
            run();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void messageNew(Integer groupId, Message message) {
        log.debug(message.toString());
        publisher.publishEvent(new NewMessageEvent(this, groupId, message));
    }

}
