package ru.java.component.event;

import com.vk.api.sdk.objects.messages.Message;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.context.ApplicationEvent;

@Data
@EqualsAndHashCode(callSuper = false)
public class NewMessageEvent extends ApplicationEvent {

    private final Integer groupId;

    private final Message message;

    public NewMessageEvent(Object source, Integer groupId, Message message) {
        super(source);
        this.groupId = groupId;
        this.message = message;
    }

}
