package ru.java.component.event;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.java.data.document.Activity;
import ru.java.data.repository.ActivityRepository;

@Slf4j
@Component
@RequiredArgsConstructor
public class ActivityTracker {

    private final ActivityRepository activityRepository;

    @Async
    @EventListener(NewMessageEvent.class)
    public void handleNewMessage(NewMessageEvent event) {
        var message = event.getMessage();
        if (message.getPeerId() > 2000000000) {
            var activity = new Activity();
            activity.setId(Activity.ActivityId.of(message.getPeerId(), message.getFromId()));
            activity.setDate(message.getDate());
            activity.setMessageId(message.getConversationMessageId());
            activityRepository.save(activity);
        }
    }

}
