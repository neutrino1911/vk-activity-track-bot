package ru.java.component.event;

import com.vk.api.sdk.objects.messages.Message;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.java.component.handler.CommandHandler;
import ru.java.config.BotProperties;

import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class NewMessageListener {

    private final BotProperties props;

    private final List<CommandHandler> handlers;

    @Async
    @EventListener(NewMessageEvent.class)
    public void handleNewMessage(NewMessageEvent event) {
        var message = event.getMessage();
        var text = message.getText();
        if (text.contains("[club" + props.getGroupId() + "|")) {
            var command = text.substring(text.indexOf(']') + 1).trim();
            handleCommand(event.getGroupId(), message, command);
        } else if (message.getPeerId().equals(message.getFromId())) {
            handleCommand(event.getGroupId(), message, text.trim());
        }
    }

    private void handleCommand(Integer groupId, Message message, String command) {
        try {
            handlers.forEach(handler -> {
                if (handler.canHandle(groupId, message, command)) {
                    handler.handle(groupId, message, command);
                }
            });
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

}
