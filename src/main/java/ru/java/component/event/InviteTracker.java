package ru.java.component.event;

import com.vk.api.sdk.objects.messages.MessageActionStatus;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.java.data.document.Chat;
import ru.java.data.repository.ChatRepository;

@Slf4j
@Component
@RequiredArgsConstructor
public class InviteTracker {

    private final ChatRepository chatRepository;

    @Async
    @EventListener(NewMessageEvent.class)
    public void handleNewMessage(NewMessageEvent event) {
        var message = event.getMessage();
        if (message.getPeerId() > 2000000000) {
            var action = message.getAction();
            if (action != null && action.getType() == MessageActionStatus.CHAT_INVITE_USER) {
                var chat = new Chat();
                chat.setPeerId(message.getPeerId());
                chatRepository.save(chat);
            } else if (chatRepository.findById(message.getPeerId()).isEmpty()) {
                var chat = new Chat();
                chat.setPeerId(message.getPeerId());
                chatRepository.save(chat);
            }
        }
    }

}
