package ru.java.component.handler;

import com.vk.api.sdk.objects.messages.Keyboard;
import com.vk.api.sdk.objects.messages.KeyboardButton;
import com.vk.api.sdk.objects.messages.KeyboardButtonAction;
import com.vk.api.sdk.objects.messages.KeyboardButtonActionType;
import com.vk.api.sdk.objects.messages.Message;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.java.component.event.NewMessageEvent;
import ru.java.data.document.Chat;
import ru.java.data.repository.ChatRepository;
import ru.java.service.ActivityService;
import ru.java.service.MessageSendService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
public class ActivityStatHandler implements CommandHandler {

    private final Set<String> commands = Set.of("стата", "статистика");

    private final ChatRepository chatRepository;

    private final ActivityService activityService;

    private final MessageSendService messageSendService;

    @Override
    public boolean canHandle(Integer groupId, Message message, String command) {
        return commands.contains(command.split(" ")[0]);
    }

    @Override
    @SneakyThrows
    public void handle(Integer groupId, Message message, String command) {
        var split = command.split(" ");
        var days = split.length > 1 ? Integer.parseInt(split[1]) : 30;
        var chats = chatRepository.findAll();
        var keyboard = new Keyboard();
        keyboard.setOneTime(true);
        keyboard.setButtons(chats
                .stream()
                .map(it -> createButton(it, days))
                .collect(Collectors.toList())
        );
        messageSendService.send(message, "Выбери чат", keyboard);
    }

    @Async
    @EventListener(NewMessageEvent.class)
    public void handleNewMessage(NewMessageEvent event) {
        var message = event.getMessage();
        if (message.getPeerId().equals(message.getFromId()) && message.getPayload() != null) {
            var payload = message.getPayload();
            if (payload.contains(".")) {
                payload = payload.substring(0, payload.indexOf('.'));
            }
            var peerId = Integer.parseInt(payload.substring(0, 10));
            var days = Integer.parseInt(payload.substring(10));
            var stat = activityService.printStat(peerId, days);
            messageSendService.send(message, stat);
        }
    }

    private List<KeyboardButton> createButton(Chat chat, int days) {
        var peerId = chat.getPeerId().toString();
        var title = chat.getTitle() != null ? chat.getTitle() : peerId;
        return List.of(new KeyboardButton().setAction(
                new KeyboardButtonAction()
                        .setType(KeyboardButtonActionType.TEXT)
                        .setLabel(title)
                        .setPayload(peerId + days)
                )
        );
    }

}
