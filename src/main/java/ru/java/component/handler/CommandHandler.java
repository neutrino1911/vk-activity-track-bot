package ru.java.component.handler;

import com.vk.api.sdk.objects.messages.Message;

public interface CommandHandler {

    boolean canHandle(Integer groupId, Message message, String command);

    void handle(Integer groupId, Message message, String command);

}
